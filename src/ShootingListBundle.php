<?php

namespace ShootingListBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;
use \Pimcore\Model\User\Permission\Definition;

class ShootingListBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/shootinglist/js/pimcore/startup.js',
            '/bundles/shootinglist/js/toolbar.js',
            '/bundles/shootinglist/js/shootingList.js',
            '/bundles/shootinglist/js/search.js'
            
        ];
    }
    
    public function getInstaller() {
        $this->getJsPaths();
        $this->getCssPaths();
        $permissionDefinition = new Definition();
        $permissionDefinition::create("plugin_ShootingList_access");
        $permissionDefinition::create("plugin_facebook_report_access");
    }
}
