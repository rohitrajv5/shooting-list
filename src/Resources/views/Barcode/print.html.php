<html>
    <head>
        <meta charset="UTF-8">        
    </head>

    <body >
        <div id="printableDiv" >
            <style type="text/css">
                body {font-size: 15px;margin: 0px;padding: 0;font-family: Arial, Helvetica, sans-serif;line-height: 20px; color: #252525; font-size: 14px;}
                .wrapper {max-width: 100%;margin: auto;padding: 5px 0;}
                .wrapper table{width: 100%;}
                .heading{font-size: 24px;border-bottom: solid 2px #ddd;}
                .PIMIDTable td{border: solid 1px #ddd; padding: 10px; font-size: 16px; line-height: 20px;}
                .PIMIDTable td span{display: block; font-size: 12px;}
                .proDetail {margin-top: 20px; font-size: 14px;} 
                .proDetail  td:first-child{ width: 40%; vertical-align: top}
                .proDetail  td img{ max-width: 100%;}
                .proDetail .detail{padding-left: 30px; vertical-align: top; line-height: 26px;}
                .proDetail .detail p{margin: 0px;}
                .spacer{margin: 20px 0;}
                .fontSmall{margin-top: 20px;}
                .fontSmall td{font-size: 12px; padding: 5px;}

                #printableDiv{
                    overflow: auto;
                    max-width: 100%;
                    max-height: 644px;
                }
                .ellipsis{
                    white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis;
                }

                .demo-1 {
                    overflow: hidden;
                    display: -webkit-box;
                    -webkit-line-clamp: 3;
                    -webkit-box-orient: vertical;
                }

                .demo-2 {
                    overflow: hidden;
                    white-space: nowrap;
                    text-overflow: ellipsis;
                    max-width: 100%;
                }
                @page {
                    size: A4;
                    margin: 1px;
                }                
                .page {
                    width: 20cm;
                    min-height: 29.7cm;
                    padding: 1cm;
                    margin: 2cm 1cm 1cm 1cm;
                    border: 1px #D3D3D3 solid;
                    border-radius: 5px;
                    background: white;
                    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
                    outline: 2cm #FFEAEA solid;
                }

                @page {
                    size: A4;
                    margin: 1px;
                }
                @media print {
                    .page {
                        margin: 0;
                        padding: 2cm;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                        outline : initial;
                    }
                }

                .clear-float{
                    clear: both;
                }
                .right-text{
                    float: right;
                }
            </style>
            <?php
            $i = 1;
            foreach ($productParents as $key => $productParent) {
                $total = 0;
                foreach ($childs[$key] as $child) {
                    $total += $child->getQuantity();
                }
                ?>
                <div class='wrapper page'>
                    <span class="right-text"><?php echo $i++; ?></span>
                    <div class="clear-float"></div>
                    <table cellpading="0" cellspacing="0">
                        <tr>
                            <td>
                                <h2 class="heading">Photography Picking Slip For PIMID: <?php echo $productParent->getId(); ?></h2>    
                            </td>
                        </tr>
                    </table>
                    <table cellpading="0" cellspacing="0" class="PIMIDTable" >
                        <tr>
                            <td>

                            </td>
                            <td>PIMID
                                <span> Meta Author</span>    
                            </td>
                            <td>
                                Colour
                                <span> Meta Description</span>

                            </td>
                            <td>
                                Complete
                                <span> Tick One Complete</span>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                1 
                            </td>
                            <td><?php echo $productParent->getId(); ?>
                            </td>
                            <td>
                                <?php echo strtoupper($productParent->getKey()); ?>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <table cellpading="0" cellspacing="0" class="proDetail">
                        <tr>
                            <td>
                                <?php
                                $image = '/e5p/images/product-placeholder.jpg';
                                if ($productParent->getRawImage()) {
                                    $image = $productParent->getRawImage();
                                }
                                ?>
                                <img src="<?php echo $image; ?>" alt="">
                            </td>
                            <td class="detail">

                                <p><strong>ID: </strong> <?php echo $productParent->getId(); ?></p>
                                <div class="spacer"></div>
                                <img src="data:image/png;base64,<?php echo $barcodes[$key]; ?>" alt="<?php echo $productParent->getEanCode(); ?>">   
                                <div class="spacer"></div>                         
                                <p><strong>EAN: </strong> <?php echo $productParent->getEanCode(); ?></p>
                                <?php if($productParent->getStyleCode()){ ?>
                                <p><strong>Style Code: </strong> <?php echo $productParent->getStyleCode()->getCode(); ?></p>
                                <?php } ?>                                
                                <p><strong>Total Qty: </strong><?php echo $total; ?></p>
                                <p><strong>Sample Image: </strong> <?php echo chunk_split((string) ($productParent->getPrimaryImage()), 40, '<br>'); ?></p>
                                <p><strong>Location: </strong> Not Available</p>

                                <p><strong>Batch ID: </strong> <?php echo $batchId; ?></p>
                                <p><strong>Status: </strong> <?php echo STATUS_ARRAY[$productParent->getStatus()]; ?></p>                                                   

                            </td>

                        </tr>
                    </table>
                    <table cellpading="0" cellspacing="0" class="PIMIDTable fontSmall">

                        <tr>
                            <td>Colour</td>
                            <?php foreach ($childs[$key] as $child) { ?>
                            <td><?php echo strtoupper($child->getKey()); ?></td>
                            <?php } ?>
                            <td>Total</td>
                        </tr>

                        <tr>
                            <td><?php echo strtoupper($productParent->getKey()); ?></td>
                            <?php foreach ($childs[$key] as $child) { ?>
                                <td><?php echo $child->getQuantity(); ?></td>
                            <?php } ?>
                            <td><?php echo $total; ?></td>
                        </tr>                    
                    </table>                    
                </div>

            <?php } ?>
        </div>
    </body>
</html>