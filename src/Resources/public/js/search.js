pimcore.registerNS("pimcore.plugin.search");
pimcore.plugin.search = Class.create(pimcore.object.search, {
    systemColumns: ["id", "fullpath", "type", "subtype", "filename", "classname", "creationDate", "modificationDate"],
    fieldObject: {},

    title: t('search_edit'),
    icon: "pimcore_icon_search",
    onlyDirectChildren: false,

    sortinfo: {},
    initialize: function (object, searchType) {
        this.object = object;
        this.element = object;
        this.searchType = searchType;
        this.noBatchColumns = [];
        this.batchAppendColumns = [];
    },

    getLayout: function () {

        if (this.layout == null) {

            // check for classtypes inside of the folder if there is only one type don't display the selection
            var toolbarConfig;

            if (this.object.data.classes && typeof this.object.data.classes == "object") {

                if (this.object.data.classes.length < 1) {
                    return;
                }

                var data = [];
                for (i = 0; i < this.object.data.classes.length; i++) {
                    var klass = this.object.data.classes[i];
                    data.push([klass.id, klass.name, ts(klass.name)]);

                }

                var classStore = new Ext.data.ArrayStore({
                    data: data,
                    sorters: 'translatedText',
                    fields: [
                        {name: 'id', type: 'number'},
                        {name: 'name', type: 'string'},
                        {name: 'translatedText', type: 'string'}
                    ]
                });


                this.classSelector = new Ext.form.ComboBox({
                    name: "selectClass",
                    listWidth: 'auto',
                    store: classStore,
                    queryMode: "local",
                    valueField: 'id',
                    displayField: 'translatedText',
                    triggerAction: 'all',
                    editable: true,
                    typeAhead: true,
                    forceSelection: true,
                    value: this.object.data["selectedClass"],
                    listeners: {
                        "select": this.changeClassSelect.bind(this)
                    }
                });

                if (this.object.data.classes.length > 1) {
                    toolbarConfig = [new Ext.Toolbar.TextItem({
                            text: t("please_select_a_type")
                        }), this.classSelector];
                } else {
                    this.currentClass = this.object.data.classes[0].id;
                }
            } else {
                return;
            }

            this.layout = new Ext.Panel({
                title: this.title,
                border: false,
                layout: "fit",
                iconCls: this.icon,
                items: [],
                tbar: toolbarConfig
            });

            if (this.currentClass) {
                this.layout.on("afterrender", this.setClass.bind(this, this.currentClass));
            }
        }

        return this.layout;
    },

    changeClassSelect: function (field, newValue, oldValue) {
        var selectedClass = newValue.data.id;
        this.setClass(selectedClass);
    },

    setClass: function (classId) {
        this.classId = classId;
        this.settings = {};
        this.getTableDescription();
    },
    getTableDescription: function () {
        Ext.Ajax.request({
            url: "/object-helper/grid-get-column-config",
            params: {
                id: this.classId,
                objectId:
                this.object.id,
                gridtype: "grid",
                gridConfigId: this.settings ? this.settings.gridConfigId : null,
                searchType: this.searchType
            },
            success: this.createGrid.bind(this, false)
        });
    },

    createGrid: function (fromConfig, response, settings, save) {

        var itemsPerPage = pimcore.helpers.grid.getDefaultPageSize(-1);
        var fields = [];

        if (response.responseText) {
            response = Ext.decode(response.responseText);

            if (response.pageSize) {
                itemsPerPage = response.pageSize;
            }

            fields = response.availableFields;
            this.gridLanguage = response.language;
            this.gridPageSize = response.pageSize;
            this.sortinfo = response.sortinfo;

            this.settings = response.settings || {};
            this.availableConfigs = response.availableConfigs;
            this.sharedConfigs = response.sharedConfigs;

            if (response.onlyDirectChildren) {
                this.onlyDirectChildren = response.onlyDirectChildren;
            }
        } else {
            itemsPerPage = this.gridPageSize;
            fields = response;
            this.settings = settings;
            this.buildColumnConfigMenu();
        }

        this.fieldObject = {};
        for (var i = 0; i < fields.length; i++) {
            this.fieldObject[fields[i].key] = fields[i];
        }

//        this.cellEditing = Ext.create('Ext.grid.plugin.CellEditing', {
//            clicksToEdit: 1,
//            listeners: {
//                beforeedit: function (editor, context, eOpts) {
//                    //need to clear cached editors of cell-editing editor in order to
//                    //enable different editors per row
//                    var editors = editor.editors;
//                    editors.each(function (editor) {
//                        if (typeof editor.column.config.getEditor !== "undefined") {
//                            Ext.destroy(editor);
//                            editors.remove(editor);
//                        }
//                    });
//                }
//            }
//        }
//        );

        var plugins = ['pimcore.gridfilters'];

        // get current class
        var classStore = pimcore.globalmanager.get("object_types_store");
        var klass = classStore.getById(this.classId);

        if (true) {
            var queryString = '';
            
            var gridHelper = new pimcore.object.helpers.grid(
                    klass.data.text,
                    fields,
                    "/object/grid-proxy?classId=" + this.classId + "&folderId=" + this.object.id + queryString,
                    {
                        language: this.gridLanguage,
                        // limit: itemsPerPage
                    },
                    false
                    );
        } else {
            var gridHelper = new pimcore.object.helpers.grid(
                    klass.data.text,
                    fields,
                    "/admin/object/grid-proxy?classId=" + this.classId + "&folderId=" + this.object.id,
                    {
                        language: this.gridLanguage,
                        // limit: itemsPerPage
                    },
                    false
                    );
        }

        gridHelper.showSubtype = false;
        gridHelper.enableEditor = true;
        gridHelper.limit = itemsPerPage;

        var propertyVisibility = klass.get("propertyVisibility");

        this.store = gridHelper.getStore(this.noBatchColumns, this.batchAppendColumns);
        if (this.sortinfo) {
            this.store.sort(this.sortinfo.field, this.sortinfo.direction);
        }
        this.store.getProxy().setExtraParam("only_direct_children", this.onlyDirectChildren);
        this.store.setPageSize(itemsPerPage);

        var gridColumns = gridHelper.getGridColumns();

        // add filters
        this.gridfilters = gridHelper.getGridFilters();

        this.languageInfo = new Ext.Toolbar.TextItem({
            text: t("grid_current_language") + ": " + (this.gridLanguage == "default" ? t("default") : pimcore.available_languages[this.gridLanguage])
        });


        this.toolbarFilterInfo = new Ext.Button({
            iconCls: "pimcore_icon_filter_condition",
            hidden: true,
            text: '<b>' + t("filter_active") + '</b>',
            tooltip: t("filter_condition"),
            handler: function (button) {
                Ext.MessageBox.alert(t("filter_condition"), button.pimcore_filter_condition);
            }.bind(this)
        });

        this.updateLaunchDateButton = new Ext.Button({
            iconCls: "pimcore_icon_filter_condition",
            text: 'Set Launch Date',
            tooltip: t("Set Launch Date"),
            handler: function (button) {
                if (this.checkProductSlection()) {
                    this.updateLaunchDate(settings);
                }

            }.bind(this)
        });

        this.curationButton = new Ext.Button({
            iconCls: "pimcore_icon_filter_condition",
            text: 'Create Batch',
            tooltip: t("Create Batch & Send for photoshoot"),
            handler: function (button) {
                if (this.checkProductSlection()) {
                    this.shootDate(settings);
                }

            }.bind(this)
        });

        this.searchBox = new Ext.form.TextField({
            name: 'nameSearch',
            listeners: {
                "change": function (field, key) {
                    var input = field;
                    if (input.getValue().length > 1) {
                        var filters = '[{"operator":"equal","value":"' + input.getValue() + '","property":"name","type":"string"}]';
                        this.store.getProxy().setExtraParam("filter", filters);
                        this.store.load();
                    } else {
//                                this.store.getProxy().setExtraParam("filter", false);
//                                this.store.load();
                    }
                }.bind(this)
            }
        });
        this.dateBox = new Ext.form.Date({
            name: 'dateSearch',
            width: 120,
            listeners: {
                "change": function (field, d) {
                    var input = field;
                    if (d instanceof Date) {
                        dateFilter = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();

                        var filters = '[{"operator":"eq","value":"' + dateFilter + '","property":"launchDate","type":"date"}]';
                        this.store.getProxy().setExtraParam("filter", filters);
                        this.store.load();
                        this.clearFilterButton.show();
                    } else if (d) {
                        var filters = '[{"operator":"eq","value":"' + d + '","property":"launchDate","type":"date"}]';
                        this.store.getProxy().setExtraParam("filter", filters);
                        this.store.load();
                        this.clearFilterButton.show();
                    } else {
                        this.store.getProxy().setExtraParam("filter", []);
                        this.store.load();
                    }
                }.bind(this)
            }
        });

        this.clearFilterButton = new Ext.Button({
            iconCls: "pimcore_icon_clear_filters",
            hidden: true,
            text: t("clear_filters"),
            tooltip: t("clear_filters"),
            handler: function (button) {
                this.grid.filters.clearFilters();
                this.toolbarFilterInfo.hide();
                this.clearFilterButton.hide();
                this.dateBox.setValue('');
            }.bind(this)
        });


        this.createSqlEditor();

        this.checkboxOnlyDirectChildren = new Ext.form.Checkbox({
            name: "onlyDirectChildren",
            style: "margin-bottom: 5px; margin-left: 5px",
            checked: this.onlyDirectChildren,
            boxLabel: t("only_children"),
            listeners: {
                "change": function (field, checked) {
                    this.grid.filters.clearFilters();

                    this.store.getProxy().setExtraParam("only_direct_children", checked);

                    this.onlyDirectChildren = checked;
                    this.pagingtoolbar.moveFirst();
                }.bind(this)
            }
        });

        var hideSaveColumnConfig = !fromConfig || save;

        this.saveColumnConfigButton = new Ext.Button({
            tooltip: t('save_grid_options'),
            iconCls: "pimcore_icon_publish",
            hidden: hideSaveColumnConfig,
            handler: function () {
                var asCopy = !(this.settings.gridConfigId > 0);
                this.saveConfig(asCopy)
            }.bind(this)
        });

        this.columnConfigButton = new Ext.SplitButton({
            text: t('grid_options'),
            iconCls: "pimcore_icon_table_col pimcore_icon_overlay_edit",
            handler: function () {
                this.openColumnConfig();
            }.bind(this),
            menu: []
        });

        this.buildColumnConfigMenu();

        // grid
        if (true)
        {
            
            if (pimcore.globalmanager.get("user").isAllowed("plugin_Report_grid")) {
                this.grid = Ext.create('Ext.grid.Panel', {
                frame: false,
                store: this.store,
                columns: gridColumns,
                columnLines: true,
                stripeRows: true,
                bodyCls: "pimcore_editable_grid",
                border: true,
                selModel: gridHelper.getSelectionColumn(),
                trackMouseOver: true,
                loadMask: true,
                plugins: plugins,
                viewConfig: {
                    forceFit: false,
                    xtype: 'patchedgridview'
                },
                listeners: {
                    celldblclick: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                        var columnName = grid.ownerGrid.getColumns();
                        if (columnName[cellIndex].text == 'ID' || columnName[cellIndex].text == 'Path') {
                            var data = this.store.getAt(rowIndex);
                            pimcore.helpers.openObject(data.get("id"), data.get("type"));
                        }
                    }
                },
                cls: 'pimcore_object_grid_panel',
                //,this.languageInfo
                tbar: [
//                    this.dateBox, "-", 
                    this.curationButton,
                    this.updateLaunchDateButton,
//                            {
//                                text: "Print Barcode",
//                                iconCls: "pimcore_icon_print",
//                                handler: function () {
//                                    this.printableObject(settings);
//                                }.bind(this)
//                            },
//                            {
//                                text: "Send For Photoshoot",
//                                iconCls: "pimcore_icon_exif",
//                                handler: function () {
//                                    this.sendForShootingList(settings);
//                                }.bind(this)
//                            },
                    "->", this.toolbarFilterInfo, this.clearFilterButton,
//                    {
//                        text: t("export_csv"),
//                        iconCls: "pimcore_icon_export",
//                        handler: function () {
//                            pimcore.helpers.csvExportWarning(function (settings) {
//                                this.exportPrepare(settings);
//                            }.bind(this));
//                        }.bind(this)
//                    }, 
//                    "-",
                    this.columnConfigButton,
                    this.saveColumnConfigButton
                ]
            });
            }else{
                this.grid = Ext.create('Ext.grid.Panel', {
                frame: false,
                store: this.store,
                columns: gridColumns,
                columnLines: true,
                stripeRows: true,
                bodyCls: "pimcore_editable_grid",
                border: true,
                selModel: gridHelper.getSelectionColumn(),
                trackMouseOver: true,
                loadMask: true,
                plugins: plugins,
                viewConfig: {
                    forceFit: false,
                    xtype: 'patchedgridview'
                },
                listeners: {
                    celldblclick: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                        var columnName = grid.ownerGrid.getColumns();
                        if (columnName[cellIndex].text == 'ID' || columnName[cellIndex].text == 'Path') {
                            var data = this.store.getAt(rowIndex);
                            pimcore.helpers.openObject(data.get("id"), data.get("type"));
                        }
                    }
                },
                cls: 'pimcore_object_grid_panel',
                //,this.languageInfo
                tbar: [
//                    this.dateBox, "-", 
                    this.curationButton,
                    this.updateLaunchDateButton,
//                            {
//                                text: "Print Barcode",
//                                iconCls: "pimcore_icon_print",
//                                handler: function () {
//                                    this.printableObject(settings);
//                                }.bind(this)
//                            },
//                            {
//                                text: "Send For Photoshoot",
//                                iconCls: "pimcore_icon_exif",
//                                handler: function () {
//                                    this.sendForShootingList(settings);
//                                }.bind(this)
//                            },
                    "->", this.toolbarFilterInfo, this.clearFilterButton,
//                    {
//                        text: t("export_csv"),
//                        iconCls: "pimcore_icon_export",
//                        handler: function () {
//                            pimcore.helpers.csvExportWarning(function (settings) {
//                                this.exportPrepare(settings);
//                            }.bind(this));
//                        }.bind(this)
//                    }, 
//                    "-",
//                    this.columnConfigButton,
//                    this.saveColumnConfigButton
                ]
            });
            }
            

        } else {

            this.grid = Ext.create('Ext.grid.Panel', {
                frame: false,
                store: this.store,
                columns: gridColumns,
                columnLines: true,
                stripeRows: true,
                bodyCls: "pimcore_editable_grid",
                border: true,
                selModel: gridHelper.getSelectionColumn(),
                trackMouseOver: true,
                loadMask: true,
                plugins: plugins,
                viewConfig: {
                    forceFit: false,
                    xtype: 'patchedgridview'
                },
                listeners: {
                    celldblclick: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                        var columnName = grid.ownerGrid.getColumns();
                        if (columnName[cellIndex].text == 'ID' || columnName[cellIndex].text == 'Path') {
                            var data = this.store.getAt(rowIndex);
                            pimcore.helpers.openObject(data.get("id"), data.get("type"));
                        }
                    }
                },
                cls: 'pimcore_object_grid_panel',
                //,this.languageInfo
                tbar: [this.languageInfo, "-", this.toolbarFilterInfo, this.clearFilterButton, "->", this.checkboxOnlyDirectChildren, "-", this.sqlEditor, this.sqlButton, "-", {
                        text: t("search_and_move"),
                        iconCls: "pimcore_icon_search pimcore_icon_overlay_go",
                        handler: pimcore.helpers.searchAndMove.bind(this, this.object.id,
                                function () {
                                    this.store.reload();
                                }.bind(this), "object")
                    }, "-", {
                        text: t("export_csv"),
                        iconCls: "pimcore_icon_export",
                        handler: function () {
                            pimcore.helpers.csvExportWarning(function (settings) {
                                this.exportPrepare(settings);
                            }.bind(this));
                        }.bind(this)
                    }, "-",
                    this.columnConfigButton,
                    this.saveColumnConfigButton
                ]
            });
        }

        this.grid.on("columnmove", function () {
            this.saveColumnConfigButton.show()
        }.bind(this));
        this.grid.on("columnresize", function () {
            this.saveColumnConfigButton.show()
        }.bind(this));

        this.grid.on("rowcontextmenu", this.onRowContextmenu);

        this.grid.on("afterrender", function (grid) {
            this.updateGridHeaderContextMenu(grid);
        }.bind(this));

        this.grid.on("sortchange", function (ct, column, direction, eOpts) {
            this.sortinfo = {
                field: column.dataIndex,
                direction: direction
            };
        }.bind(this));

        // check for filter updates
        this.grid.on("filterchange", function () {
            this.filterUpdateFunction(this.grid, this.toolbarFilterInfo, this.clearFilterButton);
        }.bind(this));

        gridHelper.applyGridEvents(this.grid);

        this.pagingtoolbar = pimcore.helpers.grid.buildDefaultPagingToolbar(this.store, {pageSize: itemsPerPage});

        this.editor = new Ext.Panel({
            layout: "border",
            items: [new Ext.Panel({
                    items: [this.grid],
                    region: "center",
                    layout: "fit",
                    bbar: this.pagingtoolbar
                })]
        });

        this.layout.removeAll();
        this.layout.add(this.editor);
        this.layout.updateLayout();

        if (save) {
            if (this.settings.isShared) {
                this.settings.gridConfigId = null;
            }
            this.saveConfig(false);
        }
    },

    /////////////////////////////////////

    checkProductSlection: function () {
        ids = [];
        var selectedRows = this.grid.getSelectionModel().getSelection();
        for (var i = 0; i < selectedRows.length; i++) {
            ids.push(selectedRows[i].data.id);
        }
        if (ids.length) {
            return true;
        } else {
            Ext.MessageBox.alert('Select Product', 'You have not selected any product.');
        }
    },
    updateLaunchDateAjax: function (button, formValues, windowShoot) {
        ids = [];
        var selectedRows = this.grid.getSelectionModel().getSelection();
        for (var i = 0; i < selectedRows.length; i++) {
            ids.push(selectedRows[i].data.id);
        }

        formValues = Ext.encode(formValues);
        button.disabled = true;
        var params = {
            classId: this.classId,
            folderId: this.element.id,
            objecttype: this.objecttype,
            "ids[]": ids,
            formValues: formValues
        };

        var options = this.options || {};
        Ext.Ajax.request({
            url: "/shooting-list/update-shoot-quantity",
            params: params,
            ignoreErrors: options.ignoreNotFoundError,
            success: function (response) {
                button.disabled = false;
                var responseData = Ext.JSON.decode(response.responseText);
                if (responseData.success) {
                    windowShoot.close();
                    pimcore.helpers.showNotification(t("success"), "Add Launch Date", "success");
                }

            }.bind(this),

            failure: function () {
                console.log('failure');
            }.bind(this)
        });
    },

    updateLaunchDate: function (settings) {
        var form = new Ext.form.FormPanel({
            bodyStyle: 'padding:10px',
            items: [
                {
                    xtype: 'datefield',
                    fieldLabel: 'Launch Date',
                    name: 'launchdate',
                    minValue: new Date()
                },
            ]
        });
        var updateButton = new Ext.Button({
            text: t("Ok"),
            handler: function (button) {
                var formValues = form.getValues();
                this.updateLaunchDateAjax(button, formValues, windowShoot);
            }.bind(this)
        });
        var windowShoot = new Ext.Window({
            modal: true,
            title: t('Set Launch Date'),
            width: 400,
            height: 150,
            bodyStyle: "padding: 10px; background:#fff;",
            buttonAlign: "center",
            shadow: false,
            closable: true,
            items: form,
            buttons: [
                updateButton,
                {
                    text: t("Close"),
                    handler: function () {
                        windowShoot.close();
                    }
                }
            ]
        });
        windowShoot.show();
    },

    shootDate: function (settings) {
        var form = new Ext.form.FormPanel({
            bodyStyle: 'padding:10px',
            items: [
                {
                    xtype: 'datefield',
                    anchor: '100%',
                    fieldLabel: 'Shoot Date',
                    name: 'shootDate',
                    allowBlank: false,
                    msgTarget: 'side',
                    minValue: new Date()
                },
            ]
        });
        var updateButton = new Ext.Button({
            text: t("Ok"),
            handler: function (button) {
                var formValues = form.getValues();
                if (formValues.shootDate != '') {
                    windowShoot.close();
                    this.printableObject(settings, formValues.shootDate);
                } else {
                    form.items.items[0].focus();
                }
                return false;
            }.bind(this)
        });
        var windowShoot = new Ext.Window({
            modal: true,
            title: t('Set Shoot Date'),
            width: 400,
            height: 150,
            bodyStyle: "padding: 10px; background:#fff;",
            buttonAlign: "center",
            shadow: false,
            closable: true,
            items: form,
            buttons: [
                updateButton,
                {
                    text: t("Close"),
                    handler: function () {
                        windowShoot.close();
                    }
                }
            ]
        });
        windowShoot.show();
    },

    printableObject: function (settings, shootDate = '') {
        var jobs = [];

        var filters = "";
        var condition = "";

        var filterData = this.store.getFilters().items;
        if (filterData.length > 0) {
            filters = this.store.getProxy().encodeFilters(filterData);
        }

        var fields = this.getGridConfig().columns;
        var fieldKeys = Object.keys(fields);

        //create the ids array which contains chosen rows to export
        ids = [];
        var selectedRows = this.grid.getSelectionModel().getSelection();
        for (var i = 0; i < selectedRows.length; i++) {
            ids.push(selectedRows[i].data.id);
        }

        settings = Ext.encode(settings);

        var params = {
            filter: filters,
            condition: condition,
            classId: this.classId,
            folderId: this.element.id,
            objecttype: this.objecttype,
            language: this.gridLanguage,
            "ids[]": ids,
            "fields[]": fieldKeys,
            settings: settings,
            shootDate: shootDate
        };

        printButton = new Ext.Button({
            text: t("Print"),
            hidden: true,
            handler: function () {
                var mywindow = window.open('', 'PRINT', 'height=400,width=600');
//                                        mywindow.document.write(responseData.html.content);
                mywindow.document.write('<html><head></head><body>' + $('#printableDiv').html() + '</body></html>');
                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();
            }.bind(this)
        });

        var options = this.options || {};
        Ext.Ajax.request({
            url: "/shooting-list/print-barcode",
            params: params,
            ignoreErrors: options.ignoreNotFoundError,
            success: function (response) {
                var responseData = Ext.JSON.decode(response.responseText);
                var barcodeContainer = new Ext.Toolbar.TextItem({
                    html: responseData.html.content
                });
                var topContainer = new Ext.container.Container({
                    layout: 'hbox',
                    padding: 1,
                    style: {
                        overflow: 'auto',
                    },
                    items: [barcodeContainer]
                }
                );

                var temp_this = this;
                var windowBarcode = new Ext.Window({
                    modal: true,
                    title: t('Print Barcode'),
                    width: 880,
                    height: 750,
                    bodyStyle: "padding: 10px; background:#fff;",
                    buttonAlign: "center",
                    shadow: false,
                    closable: true,
                    items: [topContainer],
                    buttons: [
                        printButton,
                        {
                            text: t("Print & Send for Photoshoot"),
                            handler: function (button, e) {
                                button.disabled = true;
                                button.hide();

                                var mywindow = window.open('', 'PRINT', 'height=400,width=600');
                                mywindow.document.write('<html><head></head><body>' + $('#printableDiv').html() + '</body></html>');
                                mywindow.document.close(); // necessary for IE >= 10
                                mywindow.focus(); // necessary for IE >= 10*/
                                mywindow.print();
                                mywindow.close();

                                printButton.show();
                                temp_this.sendForShootingList(settings, shootDate);
                                pimcore.layout.refresh();
                            }
                        },
                        {
                            text: t("Close"),
                            handler: function () {
                                windowBarcode.close();
                            }
                        }
                    ]
                });
                windowBarcode.show();

            }.bind(this),

            failure: function () {
                console.log('failure');
            }.bind(this)
        });


    },

    sendForShootingList: function (settings, shootDate = '') {
        var jobs = [];

        var filters = "";
        var condition = "";

        var filterData = this.store.getFilters().items;
        if (filterData.length > 0) {
            filters = this.store.getProxy().encodeFilters(filterData);
        }

        var fields = this.getGridConfig().columns;
        var fieldKeys = Object.keys(fields);

        //create the ids array which contains chosen rows to export
        ids = [];
        var selectedRows = this.grid.getSelectionModel().getSelection();
        for (var i = 0; i < selectedRows.length; i++) {
            ids.push(selectedRows[i].data.id);
        }

        settings = Ext.encode(settings);
        var params = {
            filter: filters,
            condition: condition,
            classId: this.classId,
            folderId: this.element.id,
            objecttype: this.objecttype,
            language: this.gridLanguage,
            "ids[]": ids,
            "fields[]": fieldKeys,
            settings: settings,
            shootDate: shootDate
        };

        var options = this.options || {};
        Ext.Ajax.request({
            url: "/shooting-list/add-batch",
            params: params,
            ignoreErrors: options.ignoreNotFoundError,
            success: function (response) {
                var responseData = Ext.JSON.decode(response.responseText);
                if (responseData.success) {
                    pimcore.helpers.showNotification(t("success"), "Shooting List Batch(" + responseData.batch_id + ") is Successfully Created", "success");
                    pimcore.helpers.showNotification(t("success"), "Sent for Photoshoot", "success");
                }
//
            }.bind(this),

            failure: function () {
                console.log('failure');
            }.bind(this)
        });
    }
});