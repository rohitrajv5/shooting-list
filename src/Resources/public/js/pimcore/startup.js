pimcore.registerNS("pimcore.plugin.ShootingListBundle");

pimcore.plugin.ShootingListBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.ShootingListBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("ShootingListBundle ready!");
    }
});

var ShootingListBundlePlugin = new pimcore.plugin.ShootingListBundle();
