/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) 2009-2016 pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 *
 * This file is used for adding toolbar menu configration
 *
 */

pimcore.registerNS("pimcore.plugin.toolbar");
pimcore.plugin.toolbar = Class.create({
    initialize: function () {
        //
        pimcore.plugin.broker.registerPlugin(this);
    },

    // This function add "Data Connect" menu icon in the left main menu
    pimcoreReady: function () {
        if (pimcore.globalmanager.get("user").isAllowed("plugin_ShootingList_access")) {
            this.leftNavigation(this);
        }
    },

    leftNavigation: function (scope) {
        var toolbar = pimcore.globalmanager.get("layout_toolbar");

        var perspectiveCfg = pimcore.globalmanager.get("perspective");

        var navigation = Ext.get("pimcore_navigation");
        var ulElement = navigation.selectNode('ul');
        var li = document.createElement("li");
        li.setAttribute("id", "pimcore_menu_shooting_list");
        li.setAttribute("class", "pimcore_menu_item shooting_list");
        li.setAttribute("data-menu-tooltip", t("Shooting List"));
        ulElement.appendChild(li);
        var svg = document.createElement("svg");
        svg.setAttribute("class", "pimcore_icon_multiselect");
        li.appendChild(svg);
        ulElement.appendChild(li);

        li.onclick = function () {
//            pimcore.helpers.openObject(924, "folder");
            Ext.getCmp("pimcore_panel_tree_left").collapse();
//            scope.classId = scope.getClassId(scope.configClassName);
            var options = {classId: 1};
            var tabPanel = Ext.getCmp("pimcore_panel_tabs");
            tabPanel.remove('object_report_shootingList_1', true);
            var exportScreen = new pimcore.plugin.shootingList(1, options, "shootingList");
        };

        pimcore.helpers.initMenuTooltips();
    },
    /*
     * function to get class id from class name
     * @param {type} className
     * @returns {String|data.classId}
     * get Class ID's for class names
     */
    getClassId: function (className) {
        var classId = '';
        Ext.Ajax.request({
            url: "/reports/get-class-id",
            async: false,
            params: {
                className: className,
            },
            success: function (response) {
                var data = Ext.decode(response.responseText);
                if (data.success) {
                    classId = data.classId;
                } else {
                    pimcore.helpers.showNotification(
                            "Error Occured !!",
                            data.msg,
                            "Error"
                            );
                }
            }.bind(this)
        });
        return classId;
    },
});
var toolbar = new pimcore.plugin.toolbar();