<?php

namespace ShootingListBundle\Controller;

use Pimcore\Controller\FrontendController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Pimcore\Model\DataObject;
use Picqer\Barcode;
use AppBundle\Lib\Utility;
use AppBundle\Model\Notification;
use Pimcore\Db;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Workflow;
use Pimcore\Workflow\Manager;
use Pimcore\Workflow\EventSubscriber\NotesSubscriber;

class BarcodeController extends FrontendController {

    /**
     * @Route("/shooting-list/print-barcode", name="shooting_list_print_barcode")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws Barcode\Exceptions\BarcodeException
     */
    public function printAction(Request $request) {
        $productIds = (array) $request->query->get('ids');
        $productParents = [];
        $barcodes = [];
        $childs = [];
        foreach ($productIds as $productId) {
//            $product = DataObject\Product::getById($productId);
            $productList = new DataObject\Product\Listing();
            $productList->addConditionParam("o_parentId = ?", $productId, "AND");
//            $productList->addConditionParam("photoshootDate IS NULL", [], "AND");
            $productList->addConditionParam("status IS NULL || status = 'editor_comments' || status = ?", 'new', "AND");
            $productList->load();

            $products = $productList->getObjects();
            foreach ($products as $product) {
                $productParents[$product->getId()] = $product;

                $childs[$product->getId()] = $product->getChildren([DataObject\AbstractObject::OBJECT_TYPE_VARIANT]);
            }
        }

        $generator = new Barcode\BarcodeGeneratorJPG();
        foreach ($productParents as $key => $product) {
            $barCode = $product->getEanCode();
            if ($barCode) {
                $barcodes[$key] = base64_encode($generator->getBarcode($barCode, $generator::TYPE_EAN_13));
            }
        }
        $shootDate = $request->query->get('shootDate');
        $photoshootDate = \DateTime::createFromFormat('m/d/y', $shootDate);
//        $name = date('Y-M-d', time());
        $name = $photoshootDate->format('Y-M-d');
        $batchId = $this->getUniqueName($name);
        
        $data = [
            'productParents' => $productParents,
            'barcodes' => $barcodes,
            'childs' => $childs,
            'batchId' => $batchId,
        ];
        $html = $this->render('ShootingListBundle::Barcode/print.html.php', $data);
        $response['succuess'] = true;
        $response['html'] = $html;

        return $this->json($response);
    }

    /**
     * @Route("/shooting-list/add-batch", name="shooting_list_add_batch")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function addBatchAction(Request $request, Registry $workflowRegistry, Manager $workflowManager) {
        $productIds = (array) $request->query->get('ids');
        $shootDate = $request->query->get('shootDate');
        $response = [];
        if (!$productIds) {
            return $this->json($response);
        }

        $shootingListBatchesParent = Utility::checkNCreateFolder(SHOOTING_LIST_BATCHES);
        $productList = [];
        $products = [];
        $baseProducts = [];
        foreach ($productIds as $productId) {
            $baseProduct = DataObject\Product::getById($productId);
            $baseProducts[] = $baseProduct;
            $productListing = new DataObject\Product\Listing();
            $productListing->addConditionParam("o_parentId = ?", $productId, "AND");
//            $productListing->addConditionParam("photoshootDate IS NULL", [], "AND");
            $productListing->addConditionParam("status IS NULL || status = 'editor_comments' || status = ?", 'new', "AND");
            $productListing->load();

            $productsTemp = $productListing->getObjects();
            foreach ($productsTemp as $product) {
                $products[$product->getId()] = $product;
            }
            $productList[] = new DataObject\Data\ObjectMetadata('productList', ['id'], $baseProduct);
        }
        $photoshootDate = \DateTime::createFromFormat('m/d/y', $shootDate);
//        $name = date('Y-M-d', time());
        $name = $photoshootDate->format('Y-M-d');
        $batchId = $this->getUniqueName($name);
        $batch = new DataObject\ShootingListBatch();
        $batch->setParent($shootingListBatchesParent);
        $batch->setKey($batchId);
        $batch->setBatchId($batchId);
        $batch->setProductList($productList);
        $batch->setAlbumDate($photoshootDate);
        $batch->setPublished(true);
        $batch->save();

        foreach ($products as $productId => $product) {
            $this->changeWorkflowStatus($productId, $product, $workflowRegistry, $workflowManager);
            if ($shootDate) {
                $product->setPhotoshootDate($photoshootDate);
                $product->setPhotoshootId($batchId);
                $product->save();
            }
        }
        
        foreach ($baseProducts as $product) {            
            if ($shootDate) {
                $product->setPhotoshootDate($photoshootDate);
                $product->setPhotoshootId($batchId);
                $product->save();
            }
        }
        

        foreach ($productIds as $productId) {
            $product = DataObject\Product::getById($productId);
            $product->setParent(Utility::checkNCreateFolder(PRODUCT_FOLDER));
            $product->setPublished(true);
            $product->save();
        }
        //// Notify Counting Admin ////

        $id = $batch->getId();
        Notification::sendEMail($id, 'curation_done', $products);

        $response['success'] = true;
        $response['batch_id'] = $batchId;
        return $this->json($response);
    }

    /**
     * @Route("/shooting-list/update-shoot-quantity", name="update_shoot_quantity")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function updateShootQuantityAction(Request $request) {
        $productIds = (array) $request->query->get('ids');
        $formValues = $request->query->get('formValues');
        $response = [];
        if (!$productIds || !$formValues) {
            return $this->json($response);
        }

        $formValues = json_decode($formValues, true);
        $launchdate = $formValues['launchdate'];
        $launchdate = \DateTime::createFromFormat('m/d/y', $launchdate);
        foreach ($productIds as $productId) {
            $product = DataObject\Product::getById($productId);

//            $productList = new DataObject\Product\Listing();
//            $productList->addConditionParam("o_parentId = ?", $productId, "AND");
//            $productList->addConditionParam("status IS NULL || status = ?", 'new', "AND");
//            $productList->load();
//            $products = $productList->getObjects();
//            foreach ($products as $product) {
//                $product->setLaunchDate($launchdate);
//                $product->setPublished(true);
//                $product->save();
//            }

            $product->setLaunchDate($launchdate);
            $product->setPublished(true);
            $product->save();
        }

        $response['success'] = true;
        return $this->json($response);
    }

    /**
     * @param $productId
     * @param null $product
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function changeWorkflowStatus($productId, $object = null, $workflowRegistry, $workflowManager) {
//        $object = $this->getPurchaseRequest($purchaseRequestId);
        $workflow = $workflowRegistry->get($object, 'studioWorkflow');      
        if ($workflow->can($object, 'curation_review')) {
            $object->setStatus('curation_done');
            $object->setStatusLabel(STATUS_ARRAY['curation_done']);
            $object->setPublished(true);

            $additionalData = [
                NotesSubscriber::ADDITIONAL_DATA_NOTES_COMMENT => 'Curation Done'
            ];
            $workflowManager->applyWithAdditionalData($workflow, $object, 'curation_review', $additionalData, true);
        }
    }

    /**
     * getUniqueName function
     *
     * @param string $name
     * @param string $sufix
     * @param string $folder
     * @return string
     */
    protected function getUniqueName($name, $sufix = '', $folder = SHOOTING_LIST_BATCHES) {
        static $i = 1;
        $path = $folder . '/' . $name . $sufix;
        $path = DataObject::getByPath($path);

        if ($path) {
            return $this->getUniqueName($name, "_" . $i++);
        } else {
            return $name . $sufix;
        }
    }

}
